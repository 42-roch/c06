/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 16:21:07 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/13 10:04:05 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] == s2[i]
		&& s1[i] != '\0' && s2[i] != '\0')
		i++;
	return (s1[i] - s2[i]);
}

void	sort(char **tab)
{
	int		i;
	int		a;
	char	*tmp;
	int		l;

	l = 0;
	while (tab[l])
		l++;
	a = 0;
	while (a < (l - 1))
	{
		i = a + 1;
		if (ft_strcmp(tab[a], tab[i]) > 0)
		{
			tmp = tab[a];
			tab[a] = tab[i];
			tab[i] = tmp;
			a = 0;
		}
		a++;
	}
}

int	main(int argc, char **argv)
{	
	int		i;
	int		c;
	char	*a;

	a = argv[0];
	i = 1;
	sort(&argv[1]);
	while (i < argc)
	{
		c = 0;
		while (argv[i][c])
		{
			write(1, &argv[i][c], 1);
			c++;
		}
		write(1, "\n", 1);
		i++;
	}
	return (0);
}
