/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/10 14:20:43 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/10 14:28:15 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h> 

int	main(int argc, char *argv[])
{
	int	i;
	int	a;

	i = 1;
	while (i < argc)
	{
		a = 0;
		while (argv[i][a])
		{
			write(1, &argv[i][a], 1);
			a++;
		}
		write(1, "\n", 1);
		i++;
	}
	return (0);
}
